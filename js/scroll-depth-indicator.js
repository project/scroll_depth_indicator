(function ($, Drupal) {
  Drupal.behaviors.scrollDepthIndicator = {
    attach: function (context, settings) {
      // Define indicator element
      var indicator = `
        <div id="scroll-depth-indicator-wrapper" class="scroll-depth-indicator-wrapper">
          <div class="scroll-progress"></div>
        </div>
      `;
      
      // Retrieve dynamic selector details from Drupal settings
      var dynamicSelector = drupalSettings.scroll_depth_indicator.dynamicSelector;
      
      // Get dynamic selector and type
      var selectorType = dynamicSelector.selectorType;
      var selector = dynamicSelector.selector;

      // Define dynamic selector element based on type within the context
      var progressIndicatorSelector;

      // Find the dynamic selector element within the context with different types of selectors
      switch (selectorType) {
        case 'class':
          // Prepend '.' for class selector
          progressIndicatorSelector = $(context).find('.' + selector);
          break;
        case 'element':
          // Do not prepend anything for element selector
          progressIndicatorSelector = $(context).find(selector);
          break;
        case 'id':
          // Prepend '#' for ID selector
          progressIndicatorSelector = $(context).find('#' + selector);
          break;
        default:
          progressIndicatorSelector = $(context).find(selector);
          break;
      }

      // Append the indicator element to the dynamic selector
      if (!$("#scroll-depth-indicator-wrapper").length) {
        progressIndicatorSelector.append(indicator);
      }

      // Identify the height of the web page and window
      var pageHeight = $(document).height();
      var windowHeight = $(window).height();
      var scrollTop = $(window).scrollTop();
      var scrollProgress = null;

      // Update window height on window resize
      $(window).on('resize', function() {
        windowHeight = $(window).height();
      });

      // Update the initial width of the indicator based on the scroll progress
      updateIndicatorWidth(scrollTop);

      // Attach scroll event listener to the window
      $(window).on('scroll', function () {
        // Update the width of the indicator while scrolling
        updateIndicatorWidth($(window).scrollTop());
      });

      // Function to update the width of the indicator based on the scroll progress
      function updateIndicatorWidth(scrollTop) {
        scrollProgress = Math.ceil((scrollTop / (pageHeight - windowHeight) * 100));
        $(".scroll-progress").width(scrollProgress + '%');
      }

    }
  };
})(jQuery, Drupal);
