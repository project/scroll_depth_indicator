<?php

namespace Drupal\scroll_depth_indicator\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ScrollDepthIndicatorConfigForm.
 *
 * @package Drupal\scroll_depth_indicator\Form
 */
class ScrollDepthIndicatorConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'scroll_depth_indicator_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['scroll_depth_indicator.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('scroll_depth_indicator.settings');

    // Dropdown options
    $options = [
      'element' => $this->t('Element'),
      'class' => $this->t('Class'),
      'id' => $this->t('ID'),
    ];

    $form['selector_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Selector Type'),
      '#options' => $options,
      '#default_value' => $config->get('selector_type') ?? 'element',
      '#description' => $this->t('Select the type of selector to append the scroll depth indicator.'),
      '#required' => TRUE,
    ];

    $form['selector'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Specify the selector to append the Progress indicator'),
      '#default_value' => $config->get('selector') ?? 'header',
      '#description' => $this->t('Specify the selector of the element to append the Progress indicator.'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('scroll_depth_indicator.settings')
      ->set('selector_type', $form_state->getValue('selector_type'))
      ->set('selector', $form_state->getValue('selector'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
