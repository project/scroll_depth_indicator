# Scroll Depth Indicator

The Scroll Depth Indicator displays the progress of scrolling for
end-user considering the current scroll depth of page and total length of page.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/scroll_depth_indicator).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/scroll_depth_indicator).


## Table of contents

- Requirements
- Installation
- Configuration
- Troubleshooting
- FAQ
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Go to Administration » Configuration » Scroll Depth Indicator.
2. Specify the element to append the Progress indicator.


## Troubleshooting

If the Progress indicator does not display, check the following:

1. Ensure the module is enabled.
2. Ensure the selector specified in the configuration is rendered in the DOM.


## FAQ

**Q: I want to override the height and color of progress indicator.**

**A:** In your CSS/SASS file, style the elements using the selectors,
`#scroll-depth-indicator-wrapper` for rail and
`#scroll-depth-indicator-wrapper .scroll-progress` for scroll progress.


## Maintainers

- Deepak Ramachandran Pillai - [deepakrmklm](https://www.drupal.org/u/deepakrmklm)
